package org.dramalover.dramas.services.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import org.dramalover.dramas.entities.Drama
import org.dramalover.dramas.repositories.DramaRepository
import org.junit.Test
import spock.lang.Specification

class DramaServiceImplSpec extends Specification {

    static final DRAMA_ID = 256L
    static final ANOTHER_DRAMA_ID = 128L

    @Subject
    DramaServiceImpl dramaService

    @Collaborator
    DramaRepository dramaRepository = Mock()

    @Test
    def "should get dramas from a repository"() {
        given:
            def dramas = [new Drama(), new Drama()]
            dramaRepository.findAll() >> dramas

        expect:
            dramaService.getDramas() == dramas
    }

    @Test
    def "should get dramas from a repository for the given ids"() {
        given:
            def ids = [DRAMA_ID, ANOTHER_DRAMA_ID]
        and:
            def dramas = [new Drama(), new Drama()]
            dramaRepository.findAllById(ids) >> dramas

        expect:
            dramaService.getDramas(ids) == dramas
    }

    @Test
    def "should get a drama for the given id from a repository"() {
        given:
            def drama = new Drama()
            dramaRepository.getOne(DRAMA_ID) >> drama

        expect:
            dramaService.getDramaForId(DRAMA_ID) == drama
    }

}
