package org.dramalover.dramas.facades.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import ma.glasnost.orika.MapperFacade
import org.dramalover.dramas.data.DramaData
import org.dramalover.dramas.entities.Drama
import org.dramalover.dramas.services.DramaService
import org.junit.Test
import spock.lang.Specification

class DramaFacadeImplSpec extends Specification {

    static final DRAMA_ID = 256L
    static final ANOTHER_DRAMA_ID = 128L

    @Subject
    DramaFacadeImpl dramaFacade

    @Collaborator
    DramaService dramaService = Mock()
    @Collaborator
    MapperFacade mapperFacade = Mock()

    @Test
    def "should get list of drama representations"() {
        given:
            def dramas = [new Drama(), new Drama()]
            dramaService.getDramas() >> dramas
        and:
            def dramasData = [new DramaData(), new DramaData()]
            mapperFacade.mapAsList(dramas, DramaData.class) >> dramasData

        expect:
            dramaFacade.getDramas() == dramasData
    }

    @Test
    def "should get list of drama representations for the given ids"() {
        given:
            def ids = [DRAMA_ID, ANOTHER_DRAMA_ID]
        and:
            def dramas = [new Drama(), new Drama()]
            dramaService.getDramas(ids) >> dramas
        and:
            def dramasData = [new DramaData(), new DramaData()]
            mapperFacade.mapAsList(dramas, DramaData.class) >> dramasData

        expect:
            dramaFacade.getDramas(ids) == dramasData
    }

    @Test
    def "should get drama representation for the given id"() {
        given:
            def drama = new Drama()
            dramaService.getDramaForId(DRAMA_ID) >> drama
        and:
            def dramaData = new DramaData()
            mapperFacade.map(drama, DramaData.class) >> dramaData

        expect:
            dramaFacade.getDramaForId(DRAMA_ID) == dramaData
    }

}
