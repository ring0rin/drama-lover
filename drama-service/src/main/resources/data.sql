DROP TABLE IF EXISTS dramas;

CREATE TABLE dramas (
  id INT AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(256) NOT NULL,
  broadcast_start DATE NOT NULL,
  broadcast_end DATE NOT NULL,
  network VARCHAR(32),
  episodes_number INT NOT NULL
);

INSERT INTO dramas (title, broadcast_start, broadcast_end, network, episodes_number) VALUES
  ('Descendants of the Sun', '2016-02-24', '2016-04-14', 'KBS2', 16),
  ('The Beauty Inside', '2018-10-01', '2018-11-20', 'JTBC', 16),
  ('Another Oh Hae Young', '2016-05-02', '2016-06-28', 'tvN', 18),
  ('Reply 1997', '2012-07-24', '2012-09-18', 'tvN', 16);