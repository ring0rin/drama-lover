package org.dramalover.dramas.services.impl;

import org.dramalover.dramas.entities.Drama;
import org.dramalover.dramas.repositories.DramaRepository;
import org.dramalover.dramas.services.DramaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Default implementation of {@link DramaService}.
 */
@Service
public class DramaServiceImpl implements DramaService {

    @Resource
    private DramaRepository dramaRepository;

    @Override
    public List<Drama> getDramas() {
        return dramaRepository.findAll();
    }

    @Override
    public List<Drama> getDramas(final List<Long> ids) {
        return dramaRepository.findAllById(ids);
    }

    @Override
    public Drama getDramaForId(final Long id) {
        return dramaRepository.getOne(id);
    }

}
