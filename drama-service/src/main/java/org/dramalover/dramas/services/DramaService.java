package org.dramalover.dramas.services;

import org.dramalover.dramas.entities.Drama;

import java.util.List;

/**
 * Provides methods to manage {@link Drama} information.
 */
public interface DramaService {

    /**
     * Gets all dramas.
     *
     * @return a list of dramas
     */
    List<Drama> getDramas();

    /**
     * Gets dramas for the specified {@code ids}.
     *
     * @return a list of dramas
     */
    List<Drama> getDramas(List<Long> ids);

    /**
     * Gets a drama for the given {@code id}.
     *
     * @param id a drama's id
     * @return the found drama
     */
    Drama getDramaForId(Long id);

}
