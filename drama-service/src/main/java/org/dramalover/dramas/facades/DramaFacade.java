package org.dramalover.dramas.facades;

import org.dramalover.dramas.data.DramaData;

import java.util.List;

/**
 * Provides methods to manage drama information in representation friendly format.
 */
public interface DramaFacade {

    /**
     * Gets all dramas.
     *
     * @return a list of dramas
     */
    List<DramaData> getDramas();

    /**
     * Gets dramas for the specified {@code ids}.
     *
     * @return a list of dramas
     */
    List<DramaData> getDramas(List<Long> ids);

    /**
     * Gets a drama for the given {@code id}.
     *
     * @param id a drama's id
     * @return the found drama
     */
    DramaData getDramaForId(Long id);

}
