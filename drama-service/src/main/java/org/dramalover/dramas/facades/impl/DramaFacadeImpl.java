package org.dramalover.dramas.facades.impl;

import ma.glasnost.orika.MapperFacade;
import org.dramalover.dramas.data.DramaData;
import org.dramalover.dramas.entities.Drama;
import org.dramalover.dramas.facades.DramaFacade;
import org.dramalover.dramas.services.DramaService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Default implementation of {@link DramaFacade}.
 */
@Component
public class DramaFacadeImpl implements DramaFacade {

    @Resource
    private DramaService dramaService;
    @Resource
    private MapperFacade mapperFacade;

    @Override
    public List<DramaData> getDramas() {
        final List<Drama> dramas = dramaService.getDramas();
        return mapperFacade.mapAsList(dramas, DramaData.class);
    }

    @Override
    public List<DramaData> getDramas(final List<Long> ids) {
        final List<Drama> dramas = dramaService.getDramas(ids);
        return mapperFacade.mapAsList(dramas, DramaData.class);
    }

    @Override
    public DramaData getDramaForId(final Long id) {
        final Drama drama = dramaService.getDramaForId(id);
        return mapperFacade.map(drama, DramaData.class);
    }

}
