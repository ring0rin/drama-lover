package org.dramalover.dramas.data;

import java.util.List;

/**
 * List of dramas representation.
 */
public class DramaListData {

    private List<DramaData> dramas;

    public List<DramaData> getDramas() {
        return dramas;
    }

    public void setDramas(final List<DramaData> dramas) {
        this.dramas = dramas;
    }

}
