package org.dramalover.dramas.data;

import java.util.Date;

/**
 * Drama representation.
 */
public class DramaData {

    private Long id;
    private String title;
    private Date broadcastStart;
    private Date broadcastEnd;
    private String network;
    private Integer episodesNumber;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Date getBroadcastStart() {
        return broadcastStart;
    }

    public void setBroadcastStart(final Date broadcastStart) {
        this.broadcastStart = broadcastStart;
    }

    public Date getBroadcastEnd() {
        return broadcastEnd;
    }

    public void setBroadcastEnd(final Date broadcastEnd) {
        this.broadcastEnd = broadcastEnd;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(final String network) {
        this.network = network;
    }

    public Integer getEpisodesNumber() {
        return episodesNumber;
    }

    public void setEpisodesNumber(final Integer episodesNumber) {
        this.episodesNumber = episodesNumber;
    }

}
