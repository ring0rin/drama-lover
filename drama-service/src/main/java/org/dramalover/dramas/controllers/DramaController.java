package org.dramalover.dramas.controllers;

import org.dramalover.dramas.data.DramaData;
import org.dramalover.dramas.data.DramaListData;
import org.dramalover.dramas.facades.DramaFacade;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * API for manipulating with drama information.
 */
@RestController
@RequestMapping(value = "v1/dramas")
public class DramaController {

    @Resource
    private DramaFacade dramaFacade;

    /**
     * Gets all dramas.
     *
     * @return a wrapped list of dramas
     */
    @GetMapping
    DramaListData getDramas() {
        final DramaListData dramas = new DramaListData();
        dramas.setDramas(dramaFacade.getDramas());
        return dramas;
    }

    /**
     * Gets dramas for the specified {@code ids}.
     *
     * @return a wrapped list of dramas
     */
    @GetMapping(params = "ids")
    DramaListData getDramas(@RequestParam @NotEmpty final List<Long> ids) {
        final DramaListData dramas = new DramaListData();
        dramas.setDramas(dramaFacade.getDramas(ids));
        return dramas;
    }

    /**
     * Gets a drama for the given {@code id}.
     *
     * @param id a drama's id
     * @return the found drama
     */
    @GetMapping("/{id}")
    DramaData getDrama(@PathVariable @NotBlank final Long id) {
        return dramaFacade.getDramaForId(id);
    }

}
