package org.dramalover.dramas.repositories;

import org.dramalover.dramas.entities.Drama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Domain repository that provides CRUD operations for {@link Drama} entities.
 */
@Repository
public interface DramaRepository extends JpaRepository<Drama, Long> {
}
