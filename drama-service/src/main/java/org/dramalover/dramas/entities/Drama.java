package org.dramalover.dramas.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Drama entity.
 */
@Table(name = "dramas")
@Entity
public class Drama {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Column(name = "broadcast_start")
    private Date broadcastStart;

    @Column(name = "broadcast_end")
    private Date broadcastEnd;

    private String network;

    @Column(name = "episodes_number")
    private Integer episodesNumber;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Date getBroadcastStart() {
        return broadcastStart;
    }

    public void setBroadcastStart(final Date broadcastStart) {
        this.broadcastStart = broadcastStart;
    }

    public Date getBroadcastEnd() {
        return broadcastEnd;
    }

    public void setBroadcastEnd(final Date broadcastEnd) {
        this.broadcastEnd = broadcastEnd;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(final String network) {
        this.network = network;
    }

    public Integer getEpisodesNumber() {
        return episodesNumber;
    }

    public void setEpisodesNumber(final Integer episodesNumber) {
        this.episodesNumber = episodesNumber;
    }

}
