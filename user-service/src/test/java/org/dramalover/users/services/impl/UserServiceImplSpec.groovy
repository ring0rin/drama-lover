package org.dramalover.users.services.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import org.dramalover.users.entities.User
import org.dramalover.users.repositories.UserRepository
import org.junit.Test
import spock.lang.Specification

class UserServiceImplSpec extends Specification {

    static final USER_UID = "user@test.com"

    @Subject
    UserServiceImpl userService

    @Collaborator
    UserRepository userRepository = Mock()

    @Test
    def "should get a user for the given uid from a repository"() {
        given:
            def user = new User()
            userRepository.getOne(USER_UID) >> user

        expect:
            userService.getUserForUid(USER_UID) == user
    }

}
