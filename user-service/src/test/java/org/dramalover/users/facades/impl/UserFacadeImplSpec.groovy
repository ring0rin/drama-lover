package org.dramalover.users.facades.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import ma.glasnost.orika.MapperFacade
import org.dramalover.users.data.UserData
import org.dramalover.users.entities.User
import org.dramalover.users.services.UserService
import org.junit.Test
import spock.lang.Specification

class UserFacadeImplSpec extends Specification {

    static final USER_UID = "user@test.com"

    @Subject
    UserFacadeImpl userFacade

    @Collaborator
    UserService userService = Mock()
    @Collaborator
    MapperFacade mapperFacade = Mock()

    @Test
    def "should get user representation for the given uid"() {
        given:
            def user = new User()
            userService.getUserForUid(USER_UID) >> user
        and:
            def userData = new UserData()
            mapperFacade.map(user, UserData.class) >> userData

        expect:
            userFacade.getUserForUid(USER_UID) == userData
    }

}
