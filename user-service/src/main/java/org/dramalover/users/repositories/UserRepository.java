package org.dramalover.users.repositories;

import org.dramalover.users.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Domain repository that provides CRUD operations for {@link User} entities.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
}
