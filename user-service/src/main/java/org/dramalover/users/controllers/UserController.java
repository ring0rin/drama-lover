package org.dramalover.users.controllers;

import org.dramalover.users.data.UserData;
import org.dramalover.users.facades.UserFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * API for manipulating with user information.
 */
@RestController
@RequestMapping(value = "v1/users")
public class UserController {

    @Resource
    private UserFacade userFacade;

    /**
     * Gets a user for the given {@code uid}.
     *
     * @param uid a user's uid
     * @return the found user
     */
    @GetMapping("/{uid}")
    UserData getUser(@PathVariable final String uid) {
        return userFacade.getUserForUid(uid);
    }

}
