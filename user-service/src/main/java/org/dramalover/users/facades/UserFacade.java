package org.dramalover.users.facades;

import org.dramalover.users.data.UserData;

/**
 * Provides methods to manage user information in representation friendly format.
 */
public interface UserFacade {

    /**
     * Gets a user for the given {@code uid}.
     *
     * @param uid a user's uid
     * @return the found user
     */
    UserData getUserForUid(String uid);

}
