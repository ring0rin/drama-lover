package org.dramalover.users.facades.impl;

import ma.glasnost.orika.MapperFacade;
import org.dramalover.users.data.UserData;
import org.dramalover.users.entities.User;
import org.dramalover.users.facades.UserFacade;
import org.dramalover.users.services.UserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Default implementation of {@link UserFacade}.
 */
@Component
public class UserFacadeImpl implements UserFacade {

    @Resource
    private UserService userService;
    @Resource
    private MapperFacade mapperFacade;

    @Override
    public UserData getUserForUid(final String uid) {
        final User user = userService.getUserForUid(uid);
        return mapperFacade.map(user, UserData.class);
    }

}
