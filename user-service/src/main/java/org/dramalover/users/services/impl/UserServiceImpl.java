package org.dramalover.users.services.impl;

import org.dramalover.users.entities.User;
import org.dramalover.users.repositories.UserRepository;
import org.dramalover.users.services.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Default implementation of {@link UserService}.
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    @Override
    public User getUserForUid(final String uid) {
        return userRepository.getOne(uid);
    }

}
