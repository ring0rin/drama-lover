package org.dramalover.users.services;

import org.dramalover.users.entities.User;

/**
 * Provides methods to manage {@link User} information.
 */
public interface UserService {

    /**
     * Gets a user for the given {@code uid}.
     *
     * @param uid a user's uid
     * @return the found user
     */
    User getUserForUid(String uid);

}
