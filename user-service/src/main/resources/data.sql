DROP TABLE IF EXISTS users;

CREATE TABLE users (
  uid VARCHAR(256) PRIMARY KEY NOT NULL,
  first_name VARCHAR(256) NOT NULL,
  last_name VARCHAR(256) NOT NULL
);

INSERT INTO users (uid, first_name, last_name) VALUES
  ('lucie.durand@test.com', 'Lucie', 'Durand');