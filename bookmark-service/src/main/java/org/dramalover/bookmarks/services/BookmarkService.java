package org.dramalover.bookmarks.services;

import org.dramalover.bookmarks.entities.Bookmark;

import java.util.List;

/**
 * Provides methods to manage {@link Bookmark} information.
 */
public interface BookmarkService {

    /**
     * Gets bookmarks for the given user's {@code uid}.
     *
     * @param uid a users's uid
     * @return the user's bookmarks
     */
    List<Bookmark> getBookmarksForUserUid(String uid);

}
