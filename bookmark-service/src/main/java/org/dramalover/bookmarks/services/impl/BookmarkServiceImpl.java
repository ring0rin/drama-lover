package org.dramalover.bookmarks.services.impl;

import org.dramalover.bookmarks.entities.Bookmark;
import org.dramalover.bookmarks.repositories.BookmarkRepository;
import org.dramalover.bookmarks.services.BookmarkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BookmarkServiceImpl implements BookmarkService {

    @Resource
    private BookmarkRepository bookmarkRepository;

    @Override
    public List<Bookmark> getBookmarksForUserUid(final String uid) {
        return bookmarkRepository.findByIdUserUid(uid);
    }
}
