package org.dramalover.bookmarks.controllers;

import org.dramalover.bookmarks.clients.DramaClient;
import org.dramalover.bookmarks.clients.UserClient;
import org.dramalover.bookmarks.facades.BookmarkFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static java.util.Objects.isNull;

/**
 * API for manipulating with user's bookmarks.
 */
@RestController
@RequestMapping(value = "v1/bookmarks")
public class BookmarkController {

    @Resource
    private BookmarkFacade bookmarkFacade;
    @Resource
    private UserClient userClient;
    @Resource
    private DramaClient dramaClient;

    /**
     * Gets bookmarks for the given user's {@code uid}.
     *
     * @param uid a users's uid
     * @return the user's bookmarks
     */
    @GetMapping("/{uid}")
    Object getBookmarks(@PathVariable final String uid) {
        final Object user = userClient.getUser(uid);
        if (isNull(user)) {
            throw new IllegalArgumentException(String.format("A user with uid %s not found", uid));
        }
        final List<Long> dramaIds = bookmarkFacade.getDramaIdsForUserUid(uid);
        return dramaClient.getDramas(dramaIds);
    }

}
