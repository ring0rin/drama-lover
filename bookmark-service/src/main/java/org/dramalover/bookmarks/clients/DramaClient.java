package org.dramalover.bookmarks.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * A REST client for manipulating with drama information.
 */
@FeignClient(name = "drama-service")
public interface DramaClient {

    /**
     * Gets dramas for the specified {@code ids}.
     *
     * @return a wrapped list of dramas
     */
    @GetMapping("/v1/dramas")
    Object getDramas(@RequestParam List<Long> ids);

}
