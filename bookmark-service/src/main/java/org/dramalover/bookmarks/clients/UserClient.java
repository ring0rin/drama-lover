package org.dramalover.bookmarks.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * A REST client for manipulating with user information.
 */
@FeignClient(name = "user-service")
public interface UserClient {

    /**
     * Gets a user for the given {@code uid}.
     *
     * @param uid a user's uid
     * @return the found user
     */
    @GetMapping("/v1/users/{uid}")
    Object getUser(@PathVariable String uid);

}
