package org.dramalover.bookmarks.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Bookmark entity.
 */
@Table(name = "bookmarks")
@Entity
public class Bookmark {

    @EmbeddedId
    private BookmarkId id;

    public BookmarkId getId() {
        return id;
    }

    public void setId(final BookmarkId id) {
        this.id = id;
    }

}
