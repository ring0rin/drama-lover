package org.dramalover.bookmarks.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

import static java.util.Objects.isNull;

/**
 * Bookmark entity identifier.
 */
@Embeddable
public class BookmarkId implements Serializable {

    @Column(name = "user_uid")
    private String userUid;

    @Column(name = "drama_id")
    private Long dramaId;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (isNull(o) || getClass() != o.getClass()) {
            return false;
        }
        final BookmarkId that = (BookmarkId) o;
        return userUid.equals(that.userUid)
                && dramaId.equals(that.dramaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userUid, dramaId);
    }

    public String getUserUid() {
        return userUid;
    }

    public Long getDramaId() {
        return dramaId;
    }

}
