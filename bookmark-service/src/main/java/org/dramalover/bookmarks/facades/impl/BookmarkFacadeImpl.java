package org.dramalover.bookmarks.facades.impl;

import org.dramalover.bookmarks.entities.Bookmark;
import org.dramalover.bookmarks.entities.BookmarkId;
import org.dramalover.bookmarks.facades.BookmarkFacade;
import org.dramalover.bookmarks.services.BookmarkService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link BookmarkFacade}.
 */
@Component
public class BookmarkFacadeImpl implements BookmarkFacade {

    @Resource
    private BookmarkService bookmarkService;

    @Override
    public List<Long> getDramaIdsForUserUid(final String uid) {
        return bookmarkService.getBookmarksForUserUid(uid).stream()
                .map(Bookmark::getId)
                .map(BookmarkId::getDramaId)
                .collect(Collectors.toList());
    }

}
