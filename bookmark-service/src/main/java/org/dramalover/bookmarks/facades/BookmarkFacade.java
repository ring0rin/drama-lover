package org.dramalover.bookmarks.facades;

import java.util.List;

/**
 * Provides methods to manage user's bookmarks in representation friendly format.
 */
public interface BookmarkFacade {

    /**
     * Gets drama ids for the given user's {@code uid}.
     *
     * @param uid a users's uid
     * @return the drama ids in the user's bookmarks
     */
    List<Long> getDramaIdsForUserUid(String uid);

}
