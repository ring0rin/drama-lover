package org.dramalover.bookmarks.repositories;

import org.dramalover.bookmarks.entities.Bookmark;
import org.dramalover.bookmarks.entities.BookmarkId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Domain repository that provides CRUD operations for {@link Bookmark} entities.
 */
@Repository
public interface BookmarkRepository extends JpaRepository<Bookmark, BookmarkId> {

    /**
     * Finds bookmarks by {@code userUid}.
     *
     * @param userUid a user's uid
     * @return a list of user's bookmarks
     */
    List<Bookmark> findByIdUserUid(String userUid);

}
