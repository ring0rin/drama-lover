DROP TABLE IF EXISTS bookmarks;

CREATE TABLE bookmarks (
  user_uid VARCHAR(256) NOT NULL,
  drama_id INT NOT NULL,
  PRIMARY KEY(user_uid, drama_id)
);

INSERT INTO bookmarks (user_uid, drama_id) VALUES
  ('lucie.durand@test.com', 1),
  ('lucie.durand@test.com', 4);