# configure Docker Envrionment to use Docker Vm
eval $(docker-machine env default --shell linux)

### set docker env
eval $(minikube docker-env)

### build the repository
#mvn clean install

### build the docker images on minikube
docker build -t user-service user-service/.
docker build -t drama-service drama-service/.
docker build -t bookmark-service bookmark-service/.

### user-service
kubectl delete -f user-service/user-service-deployment.yaml
kubectl create -f user-service/user-service-deployment.yaml

### drama-service
kubectl delete -f drama-service/drama-service-deployment.yaml
kubectl create -f drama-service/drama-service-deployment.yaml

### bookmark-service
kubectl delete -f bookmark-service/bookmark-service-deployment.yaml
kubectl create -f bookmark-service/bookmark-service-deployment.yaml

### roles
kubectl delete rolebinding default:service-discovery-client
kubectl delete -f drama-lover-roles.yaml
kubectl create -f drama-lover-roles.yaml
kubectl create rolebinding default:service-discovery-client --clusterrole service-discovery-client --serviceaccount default:default

### ingress
kubectl delete -f drama-lover-ingress.yaml
kubectl create -f drama-lover-ingress.yaml

### fluentd
kubectl delete -f drama-lover-fluentd.yaml
kubectl create -f drama-lover-fluentd.yaml

### check that the pods are running
kubectl get pods