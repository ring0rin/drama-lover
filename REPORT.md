## Task

One of the hardest parts of the course is to get to understand what _exactly_ needs to be done in practice.

I've only summarized what was written in the .pdf task and what was said in the practice video and made it as I understood it.

## What Was Done

I've created a mini-project from the scratch for drama-lovers (as me). It contains 3 microservices:

1. `user-service` - holds information about users.
2. `drama-service` - manages drama data.
3. `bookmark-service` - process user's bookmarks (must what dramas).

The whole system is based on K8S. Each microservice has its own deployment configuration. All deployment commands are described in `drama-lover-deployment.sh`.

In general, there were configured:

1. Service discovery (Kubernetes).
2. Load balancing (Ribbon).
3. API gateway (Ingress).
4. Log aggregation (Fluentd).

Also, there were used:

1. Feign - to simplify HTTP API clients.
2. Spock - for writing tests.
3. Orika - to map DB entities to lightweight data objects.

## What could be better

1. To "Understand cloud deployments aspects via K8S" need have a separate lecture. It took an enormous amount of time to understand how to configure it and why it wasn't working.
2. Contact, that might answer some questions during the course, was not presented.
3. The task is unclear - in the document one information, in the video another. Worth to make it a bit clear.
4. It would be worthwhile to have several small projects related to each lesson. Or, for example, one bigger project that could be done during the course.
